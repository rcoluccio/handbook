
When you attend a meeting with the customer you are Agile Lab, when you answer to an email you are Agile Lab, when you do promises you are Agile Lab. Everything is a matter of ownership and responsability


# Agile Lab  team-members

1. At Agile Lab leadership is requested from everyone.
2. As a leader, Agile Lab team-members will follow your behavior, so always do the right thing.
3. Everyone that joins Agile Lab should consider themselves ambassadors of our values and protectors of our culture.
Behavior should be consistent inside and outside the company, just do the right thing inside the company, and don't fake it outside.
4. Agile Lab respects your judgment of what is best for you, since you know yourself best. If you have a better opportunity somewhere else don't stay at Agile Lab out of a sense of loyalty to the company.
5. In tough times people will put in their best effort when they are doing it for each other.
6. We work async. Lead by example and make sure people understand that things need to be written down as they happen.
7. It is encouraged to disagree and have constructive debates but please argue intelligently.
8. We value truth seeking over cohesion.
9. Start meetings on time, be on time yourself, don't ask if everyone is there, and don't punish people that have shown up on time by waiting for people or repeating things for those that come late. When a meeting unblocks a process or decision, don't celebrate that but instead address the question: How can we unblock in the future without needing a meeting?
10. We give feedback, lots of it. Don't hold back on suggestions for improvements.
11. Strive to make the organization simpler.
12. Saying something to the effect of "as you might have heard", "unless you've been living in a cage you know", "as everyone knows", or "as you might know" is toxic. The people that know don't need it to be said. The people that don't know feel like they missed something and might be afraid to ask about the context


# Agile Lab team-leaders

1. Managing underperformance is one of your most important tasks as a manager.
2. When times are great, be a voice of moderation. When times are bad, be a voice of hope.
3. If you praise someone, try to do it publicly and in front of an audience. If you give suggestions to improve, do it privately 1 on 1.
4. Understand that there are different ways to get to the same goal. There are different perspectives, and discussions need to happen.
5. When someone says they are considering quitting, drop everything and listen to them. Ask questions to find out what their concerns are. If you delay, the person will not feel valued and the decision will be irreversible.
6. If you are asked why someone has left or is leaving, please refer that person to the general guidelines section of the handbook where we describe what can and cannot be shared.
7. People should not be given a raise or a title because they ask for it or threaten to quit. We should pro-actively give raises and promote people without people asking. If you do it when people ask, you are being unfair to people that don't ask and you'll end up with many more people asking.
8. Don't refer to Agile Lab as a family. It is great that our team feels like a close-knit group and we should encourage that, as this builds a stronger team. But families and teams are different. Families come together for the relationship and do what is critical to retain it. Teams are assembled for the task and do what is required to complete it. Don't put the relationship above the task. Besides, families don't have an offboarding process. Families should have unconditional love, while teams have conditional love. The best companies are supporters of families.
9. Praise and credit the work of your reports to the rest of the company, never present it as your own. 
10. Try to be aware of your cognitive biases.
11. Combine consistency and agility.
12. Do everything to unblock people. If someone has a question that is keeping them from being productive, try to answer the question yourself or find someone who can.
13. "People either get shit done or they don't. And it's easy to be tricked because they can be smart but never actually do anything." Watch for results instead of articulate answers to questions, otherwise you'll take too much time identifying under-performers.
14. We don't have explicit 20% time at Agile Lab. We measure results and not hours.
15. "Always tell us the bad news promptly. It is only the good news that can wait." Make sure to inform your manager of bad news as quickly as possible. Promptly reporting bad news is essential to preserving the trust that is needed to recover from it.
16. Complain up and explain down. Raise concerns you hear to your manager. When peers or reports complain, explain why a decision was made. If you don't understand why, ask your manager.
