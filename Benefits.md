

All the benefits listed below are available for full time employees, they are not valid for external contractors.

# Conferences

Conferences are great resource to feed the technical growth and to stimulate the desire to try new technologies or to implement new solutions. The comparison with what others do is always positive.

You can attend to one international ( Europe ) conference per year. The conference must be inherent to the spectrum of technologies and interests of Agile Lab.
Agile Lab will pay for:
- Trasportations
- Accomodation ( max 3 nights )
- Conference Ticket
- Food ( max  2 days )

The first two days of the conference ( most of them are only 2 days ) are considered as normal working days, excess days will be considered holidays.


# Smart Working

We noticed that productivity si raising in a quiet environment without distractions and reducing the stress of trasportations can release lot of additional energies. Then if you need to have to focus on something you are encouraged to take a smartworking day.

You can enable smartworking, asking to "amministrazione" and countersigning a specific policy document. This entitles you to one day a week of smartworking. This can be extended to two days per week if you trip home-office-home is more that 1.5 hours per day.
Everytime you want to schedule a smartworking day, just send an email notifying your manager, without any need of a resonse from him.


# Profit Sharing

Agile Lab wants to share results with employees as much as possible to have them motivated and owner of what they do along the year.
If the company reach the goal in terms of profitability, set by the CEO at the begin of the year, everyone is receiving a proportional share of it. Also this quota is defined at the beginning of the year by the CEO.


# Certifications

IT certifications are an other great way to demonstrate our excellence in a specific field, so employees are encouraged to do them as much as possible. Agile Lab will cover all the costs.





