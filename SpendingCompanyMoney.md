

Spend company money like it is your own money. No, really.

# Office Equipment

The easiest way to buy something for the office is to ask for them to the Facility Manager of your office, he has a company credit card and he is responsible for it. He will take care also of invoice archiving. Otherwise if you are a remote worker or you need something that is not in the perimeter of the facility manager, you can refer to peopleops@agilelab.it

Office Equipment and Supplies. The company will provide the following items and reimburs for them in case of need. Office equipment should be used for business: 


**Hardware**

- External monitor
- Cables
- Webcam
- Headphones
- Keyboard
- Mouse/Trackpad
- Laptop stand



**Software**

We use Microsoft Office 365, typically the online office suite is enough, but if you need the desktop suite to do your job properly, just ask. 
If you need an additional software that requires a license do the following steos:
*   ask to your collegues if this software can solve also their needs and collect feedbacks
*   send an email to peopleops@agilelab.it explaining why it is important and how many people need it.  



**Business cards** 

this must be ordered emailing peopleops@agilelab.it



**Work-related books** 

In Agile Lab we have an internal library, that you can find in "Big Data" Sharepoint --> Library.
If you want to buy a new book just ask to the Facility Manager.



**Coffe**

Coffe is free !!!


# Expenses

This is mainly referred to work trip expenses.
Please arrange yourself the best solution, don't ask to "peopleops" to organize the travel for you, peopleops will only pay for it.
If you have time to plan a trip, do it in advance to reduce costs as you do with your personal trips.
You don't need authorizations unless you have to bypass soft limits, so don't ask for them.


**Car**

If you use your personal car for a work trip, the mileage ( KM ) is reimbursed according to local law. 
If you don't own a car and you think the best solution is to rent one, just do it.


**Launch/Dinner**

No limits. ( Spend company money like it is your own money )


**Hotel**

Soft limit at 100 Euro/Night. 
If you need to bypass the soft limit, ask to peopleops@agilelab.it for an approval.


**Transportations**

Soft limit is 500 Euro overall for transportations.
If you need to bypass the soft limit, ask to peopleops@agilelab.it for an approval.


