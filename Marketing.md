
The Marketing team focuses its activities on building and reinforcing brand awareness, promoting contents and creating new business opportunities using both traditional and digital channels. 
To share information within our team, we have created a specific circle called “Community” who is in charge of supporting the marketing resources in preparing contents and giving a contribution in the set up of field marketing activities, but everyone can provide with new ideas and suggestions.


# Website

Our website (https://www.agilelab.it/) provides information about our business, solutions and services, team, values and news.
The marketing team is in charge of the website update, but everyone is expected to contribute with suggestions on new topics, changes or improvements to be held for a better digital communication. 
The page News is updated with events, new contents, links to magazines articles etc.


# Social media

The social media company accounts (Linkedin and Twitter) are managed either by the Marketing team and by some members of the Community Circle.
Everyone is personally responsible for messages posted on social media and everyone must remember that is representing Agile Lab, its culture and its values when interacts with the company account. To share our values and experiences, everyone is expected to contribute by sharing, commenting and engaging with Agile Lab updates.
In the case of events and conferences attendance, everyone is required to share some photos and information.


# Events

We can be involved in different kinds of events for sharing knowledge and networking:
-	Meetup: tech events where are normally hosted talks to discuss, explore and deepen some topics related to technology, methodology or developed use cases.
The Meetups are managed by the Marketing team together with the Community Circle Members, one for each company location.
-	Workshop: this kind of event has the aim to share use cases experience, news regarding innovative technologies and methodologies and to promote Agile Lab solutions.
-	3rd party events: sponsorship of events focused on specific technologies, innovative business topics, recruiting days.
-	Informative workshop: Agile Lab members are often involved in workshops, discussion panels, round tables set up by University or Research Organizations.


# Content marketing

Content production is a priority activity to build the image of our company. The key focus are: innovation, passion for technology, competence and agile thinking.
The default language is English, with some exceptions for Italian in specific situations.
The activity is held both by the Marketing team and by the members of the Community Circle: the first is in charge of preparing corporate contents (presentations, company profiles, videos, event invitations, advertorials, newsletter), while the second writes the tech blog posts on a basis of a common agenda shared at the beginning of each quarter and managed together during the period. 
In summer 2019 we have launched a publication on Medium “Agile Lab Engineering” where we go deeper with some topics related essentially to technology and interesting use cases.


# Press

The Marketing Team is linked with Media Agencies to find the best opportunities for communicating our values and experiences, on IT magazines and papers.
